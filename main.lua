phi = 0

pos = {}
pos.x = 0
pos.y = 0

lastDt = 0

pause = false


width = 640/2
height = 480/2

scaleX = 1920 / width
scaleY = 1200 / height


step = 0.1


love.keyboard.setKeyRepeat( true )

function render(p, phi, height, horizon, scale_height, distance, screen_width, screen_height)

    sinphi = math.sin(phi)
    cosphi = math.cos(phi)

    yBuffer = {}

    for i = 0, screen_width do
        yBuffer[i] = screen_height
    end

    dz = 1
    z = step    

    clip = heightmapData:getWidth()-1

    while z < distance 
    do      
        pleft = {}
        pleft.x = ( -cosphi * z - sinphi * z ) + p.x
        pleft.y = (  sinphi * z - cosphi * z ) + p.y 
        
        pright = {}        
        pright.x = ( cosphi * z - sinphi * z) + p.x 
        pright.y = (-sinphi * z - cosphi * z) + p.y
        
        dx = (pright.x - pleft.x) / screen_width
        dy = (pright.y - pleft.y) / screen_width

        for i=0,screen_width do        
            col = heightmapData:getPixel(pleft.x%clip,pleft.y%clip)      
            height_on_screen = (height - col*256) / z * scale_height +  horizon
   
            if height_on_screen < yBuffer[i] then
                r,g,b,a = mapData:getPixel(pleft.x%clip, pleft.y%clip)
                love.graphics.setColor(r,g,b,a)  
                love.graphics.line(i, height_on_screen, i, yBuffer[i] )                     
                yBuffer[i] = height_on_screen
            end
            
            pleft.x = pleft.x + dx;
            pleft.y = pleft.y + dy;
        end
        z = z + dz
        dz = dz + step
    end

end

function love.load(...)            
    mapData = love.image.newImageData( "map.png" )
    map = love.graphics.newImage( mapData )
    heightmapData = love.image.newImageData( "heightmap.png" )
    heightmap = love.graphics.newImage( heightmapData )
    love.window.setMode( width*scaleX, height*scaleY)
end

function love.keypressed(key, scancode, isrepeat)
    if key == "escape" then
       love.event.quit()
    end

    if key == "space" then
        pause = not pause
     end

    if key == "+" then
        print(phi)
        phi = (phi + 10) 
    end
 
    if key == "-" then
        print(phi)
        phi = (phi - 10) 
    end

    if key == "w" then
        pos.y = pos.y - 1 
    end

    if key == "s" then
        pos.y = pos.y + 1 
    end

    if key == "j" then
        distance = distance + 10
    end

    if key == "k" then
        distance = distance - 10
    end

   
end

function love.update(dt)

    if( not pause) then
        phi = phi + 0.1
    end

    lastDt = lastDt + dt
    if lastDt>1 then
        lastDt = 0
    end

    if phi >= 360 then
        phi = 0
    end

    if phi <= -360 then
        phi = 0
    end
  
end

function love.draw()
    love.graphics.clear({135/255, 206/255, 235/255,0})
    love.graphics.scale(scaleX, scaleY)
    render(pos, phi, 150, 100, 120, 250, width, height) 
    love.window.setTitle("FPS: " .. love.timer.getFPS())
end